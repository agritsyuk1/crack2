#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a target hash, crack it. Return the matching
// password.
char * crackHash(char *target, char *dictionary)
{
    // Open the dictionary file
    FILE *ry = fopen(dictionary, "r");
    // Loop through the dictionary file, one line
    // at a time.
    char word[50];
    while(fgets(word, 50, ry) != NULL)
    {
        char *nl = strchr(word, '\n');
        if (nl != NULL)
            *nl = '\0';
        char *hash = md5(word, strlen(word));
        if (strcmp(hash, dictionary) == 0)
        {
            printf("%s %s\n", hash, word);
        }
        free(hash);
    }
    // Hash each password. Compare to the target hash.
    // If they match, return the corresponding password.
    
    // Free up memory?
    
    return NULL;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Open the hash file for reading.
    FILE *ry = fopen(argv[2], "r");
    if (ry == NULL)
    {
        perror("Can't open file");
        exit(1);
    }
    // For each hash, crack it by passing it to crackHash
    char word[50];
    while(fgets(word, 50, ry) != NULL)
    {
        char *nl = strchr(word, '\n');
        if (nl != NULL)
            *nl = '\0';
        //printf("%s\n", word);
        char *hash = md5(word, strlen(word));
        if (strcmp(hash, argv[1]) == 0)
        {
            printf("%s %s\n", hash, word);
        }
        free(hash);
    }
    // Display the hash along with the cracked password:
    //   5d41402abc4b2a76b9719d911017c592 hello
    
    // Close the hash file
    fclose(ry);
    // Free up any malloc'd memory?
}
